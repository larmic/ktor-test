# ktor-test

Demo project to learn how gitlab ci works. 

## Used technologies

* GitLab
* Kotlin 1.4.21
* Ktor 1.4.3
* JUnit 5.7.0

## Requirements

* Java 11
* Maven >= 3.2.1 (Kotlin comes as maven dependency)

##### Clone repository and build project

```ssh
$ git clone https://gitlab.com/larmic/ktor-test
$ mvn clean package
$ java -jar target/ktor-*-with-dependencies.jar
$ curl localhost:8080 
```